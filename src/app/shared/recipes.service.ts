import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class RecipesService {
  recipesChange = new Subject<Recipe[]>();
  recipesFetching = new Subject<boolean>();
  recipesUploading = new Subject<boolean>();
  recipeRemoving = new Subject<boolean>();

  private recipesArray: Recipe[] = [];

  constructor(private http: HttpClient) {}

  fetchRecipes() {
    this.recipesFetching.next(true);
    this.http.get<{[id: string]: Recipe}>('https://kamilya-61357-default-rtdb.firebaseio.com/recipes.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const recipeData = result[id];
          return new Recipe(
            id,
            recipeData.name,
            recipeData.description,
            recipeData.image,
            recipeData.ingredients,
            recipeData.steps
          );
        });
      }))
      .subscribe(recipes => {
        this.recipesArray = recipes;
        this.recipesChange.next(this.recipesArray.slice());
        this.recipesFetching.next(false);
      }, () => {
        this.recipesFetching.next(false);
      });
  }

  fetchRecipe(id: string) {
    return this.http.get<Recipe | null>(`https://kamilya-61357-default-rtdb.firebaseio.com/recipes/${id}.json`)
      .pipe(map(result => {
        if(!result) {
          return null;
        }
        return new Recipe(
          id,
          result.name,
          result.description,
          result.image,
          result.ingredients,
          result.steps
        );
      }));
  }

  addNewRecipe(recipe: Recipe) {
    const body = {
      name: recipe.name,
      description: recipe.description,
      image: recipe.image,
      ingredients: recipe.ingredients,
      steps: recipe.steps
    };
    this.recipesUploading.next(true);
    return this.http.post('https://kamilya-61357-default-rtdb.firebaseio.com/recipes.json', body)
      .pipe(tap(() => {
        this.recipesUploading.next(false);
      }, () => {
        this.recipesUploading.next(false);
      }));
  }

  removeRecipe(id: string) {
    this.recipeRemoving.next(true);
    return this.http.delete(`https://kamilya-61357-default-rtdb.firebaseio.com/recipes/${id}.json`)
      .pipe(tap(() => {
        this.recipeRemoving.next(false);
      }, () => {
        this.recipeRemoving.next(false);
      }));
  }
}
