export class Recipe {
  isRemoving = false;
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public image: string,
    public ingredients: string,
    public steps: {stepImage: string, stepDescription: string}[],
  ) {}
}
