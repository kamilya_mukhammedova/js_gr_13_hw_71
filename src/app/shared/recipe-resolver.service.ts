import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Recipe } from './recipe.model';
import { EMPTY, Observable, of } from 'rxjs';
import { RecipesService } from './recipes.service';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipeResolverService implements Resolve<Recipe> {
  constructor(
    private recipesService: RecipesService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipe> | Observable<never> {
    const recipeId = <string>route.params['id'];
    return this.recipesService.fetchRecipe(recipeId).pipe(mergeMap(recipe => {
      if(recipe) {
        return of(recipe);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
