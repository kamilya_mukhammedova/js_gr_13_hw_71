import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesListComponent } from './recipes-list/recipes-list.component';
import { NewRecipeComponent } from './new-recipe/new-recipe.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';
import { RecipeResolverService } from './shared/recipe-resolver.service';

const routes: Routes = [
  {path: '', component: RecipesListComponent},
  {path: 'add-new-recipe', component: NewRecipeComponent},
  {
    path: 'recipes/:id',
    component: RecipeDetailsComponent,
    resolve:
      {recipe: RecipeResolverService}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
