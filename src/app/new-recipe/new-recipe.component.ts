import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { RecipesService } from '../shared/recipes.service';
import { Recipe } from '../shared/recipe.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.css']
})
export class NewRecipeComponent implements OnInit, OnDestroy {
  recipeForm!: FormGroup;
  recipesUploadingSubscription!: Subscription;
  isUploading = false;
  stepButtonIsClicked = false;

  constructor(
    private recipesService: RecipesService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.recipeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      image: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([], Validators.required)
    });
    this.recipesUploadingSubscription = this.recipesService.recipesUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  addSteps() {
    const steps = <FormArray>this.recipeForm.get('steps');
    const stepsGroup = new FormGroup({
      stepDescription: new FormControl('', Validators.required),
      stepImage: new FormControl('', Validators.required)
    });
    steps.push(stepsGroup);
    this.stepButtonIsClicked = true;
  }

  getStepsControls() {
    const steps = <FormArray>this.recipeForm.get('steps');
    return steps.controls;
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.recipeForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  stepsControlHasError(fieldName: string, index: number, errorType: string) {
    const steps = <FormArray>this.recipeForm.get('steps');
    const stepsArray = <FormGroup>steps.controls[index];
    const stepsControl = stepsArray.get(fieldName);
    return Boolean(stepsControl && stepsControl.touched && stepsControl.errors?.[errorType]);
  }

  formIsInvalid() {
    return Boolean(this.recipeForm.invalid);
  }

  saveRecipe() {
    const id =  Math.random().toString();
    const newRecipe = new Recipe(
      id,
      this.recipeForm.value.name,
      this.recipeForm.value.description,
      this.recipeForm.value.image,
      this.recipeForm.value.ingredients,
      this.recipeForm.value.steps
    );
    this.recipesService.addNewRecipe(newRecipe).subscribe(() => {
      void this.router.navigate(['/']);
    });
  }

  ngOnDestroy(): void {
    this.recipesUploadingSubscription.unsubscribe();
  }
}

