import { Component, OnDestroy, OnInit } from '@angular/core';
import { Recipe } from '../shared/recipe.model';
import { RecipesService } from '../shared/recipes.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit, OnDestroy {
  recipes!: Recipe[];
  recipesChangeSubscription!: Subscription;
  recipesFetchingSubscription!: Subscription;
  recipeRemovingSubscription!: Subscription;
  isFetching = false;
  isRemoving = false;

  constructor(private recipesService: RecipesService) { }

  ngOnInit(): void {
    this.recipesChangeSubscription = this.recipesService.recipesChange.subscribe((recipes: Recipe[]) => {
      this.recipes = recipes;
    });
    this.recipesFetchingSubscription = this.recipesService.recipesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.recipeRemovingSubscription = this.recipesService.recipeRemoving.subscribe((isRemoving: boolean) => {
      this.isRemoving = isRemoving;
    });
    this.recipesService.fetchRecipes();
  }

  onRemove(id: string, index: number) {
    this.recipes[index].isRemoving = true;
    this.recipesService.removeRecipe(id).subscribe(() => {
      this.recipesService.fetchRecipes();
    });
  }

  ngOnDestroy(): void {
    this.recipesChangeSubscription.unsubscribe();
    this.recipesFetchingSubscription.unsubscribe();
    this.recipeRemovingSubscription.unsubscribe();
  }
}
